import React from 'react';

export default function HolaMundo() {
    return(
        <div>
            <h1> Hola mundo </h1>
            <h2> Oscar Moreno </h2>
        </div>
    );
}

export function Adios() {
    return(
        <div>
            <h3> Adios </h3>
        </div>
    );
}
